all: main.pdf

%.aux: %.tex
	xelatex -shell-escape $<

%.blg: %.aux 
	bibtex ${@:.blg=}

%.pdf: %.tex %.blg
	xelatex -shell-escape $<
	xelatex -shell-escape $<

clean:
	rm -f *pdf *bbl *aux *log *blg


